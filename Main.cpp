#include <iostream>
#include <algorithm>

class Player {
public:
    std::string name;
    int score;
};

bool comparePlayers(const Player& player1, const Player& player2) {
    return player1.score > player2.score;
}

int main() {
    int numPlayers;
    std::cout << "Enter number of players: ";
    std::cin >> numPlayers;

    Player* players = new Player[numPlayers];

    for (int i = 0; i < numPlayers; i++) {
        std::cout << "Enter name of player " << i + 1 << ": ";
        std::cin >> players[i].name;
        std::cout << "Enter score of player " << players[i].name << ": ";
        std::cin >> players[i].score;
    }

    std::sort(players, players + numPlayers, comparePlayers);

    std::cout << "Leaderboard:\n";
    for (int i = 0; i < numPlayers; i++) {
        std::cout << "Player " << players[i].name << " have " << players[i].score << " points\n";
    }

    delete[] players;

}